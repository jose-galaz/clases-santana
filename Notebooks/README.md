# Machine Learning Course - Setup Instructions

## Roberto Santana, University of the Basque Country.

1. Install `miniconda` for Python 3.x for your OS from https://docs.conda.io/en/latest/miniconda.html.

2. Using your terminal, install course dependencies by running:

```bash
conda install -c conda-forge tpot sklearn jupyter matplotlib
```

3. Using your terminal, `cd` the location of the courses in your HDD and run:

```bash
jupyter notebook
```
**Important:** Make a local copy of the notebooks in your HDD, do not run Jupyter on a shared folder (i.e. Google Drive). 